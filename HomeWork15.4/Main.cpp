#include <iostream>
#include <clocale>

int const N = 10;

void PrintOddEven(bool IsEven, int N)
{
	int count;
	if (IsEven)
	{
		std::cout << "������ ����� �� 0 �� " << N << " : ";
		count = 0;
	}
	else
	{
		std::cout << "�� ������ ����� �� 0 �� " << N << " : ";
		count = 1;
	}
	while (count <= N)
	{
		std::cout << count << "  ";
		count += 2;
	}
	std::cout << "\n";
}

int main()
{
	setlocale(LC_CTYPE, "rus");

	std::cout << "������ ����� �� 0 �� " << ::N << ": ";
	for (int i = 0; i <= N; i += 2)
	{
		std::cout << i << " ";
	}
	std::cout << "\n";

	PrintOddEven(true, 20);
	PrintOddEven(false, 20);
}
